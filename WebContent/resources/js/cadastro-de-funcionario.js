new Vue({
	el:"#cadastro",
	data: {
		funcionarioSetor: {
			id: '',
			nome: '',
			salario: '',
			email: '',
			dataNascimento: '',
			idSetor: '',
			nomeSetor: ''
	},
		listaSetores: [],
		selecionado: null
		
	},
	created : function(){
		
		const vm = this;
		
		axios.get("/funcionarios/rs/funcionarios/todosSetores")
			.then(response => {
				vm.listaSetores = response.data;
			}).catch(function (error) {

			}).finally(function() {
			});
			
	},
	methods :{
		salvar: function(){
			const vm = this;
			
			if(vm.selecionado == null){
				alert("Preecha o campo setor!");
				return;	
			} else {
				vm.funcionarioSetor.idSetor = vm.selecionado;
				axios.post("/funcionarios/rs/funcionarios/cadastro", vm.funcionarioSetor)
				.then(response => {
					alert("Funcionário cadastrado com sucesso!");
				}).catch(function (error) {
					vm.salvar();
				}).finally(function() {
				});
			}
		
			
		}
	
	}


})