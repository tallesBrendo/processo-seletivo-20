var inicio = new Vue({
	el:"#inicio",
    data: {
        lista: [],
		listaSetores:[],
		selecionado: "",
		funcionarioAlterado: {
			id: '',
			nome: '',
			salario: '',
			email: '',
			dataNascimento: '',
			idSetor: '',
			nomeSetor: ''
		},
		index: null
    },
    created: function(){
        let vm =  this;
        vm.listarFuncionarios();
		
		//abreLoad();

		axios.get("/funcionarios/rs/funcionarios/todosSetores")
			.then(response => {
				vm.listaSetores = response.data;
		//		fechaLoad();
			}).catch(function (error) {
			}).finally(function() {

			});
    },
    methods:{
	//Busca os itens para a lista da primeira página
        listarFuncionarios: function(){
			const vm = this;

			axios.get("/funcionarios/rs/funcionarios/listarFuncionarios")
			.then(response => {
				vm.lista = response.data;
				console.log(vm.lista)

			}).catch(function (error) {
			}).finally(function() {

			});

		},
		apagar: function(user){
			const vm = this;
			axios.get("/funcionarios/rs/funcionarios/apagaFuncionario/"+user.id)
			.then(response => {
				alert("Funcionário deletado com sucesso!");
			}).catch(function (error) {
			}).finally(function() {
				vm.listarFuncionarios();
			});
		},
		habilitaEdicao: function(funcionario, index){
			const vm = this;
			vm.index = index;
			vm.funcionarioSelecionado = funcionario;
			vm.funcionarioAlterado.id = funcionario.id;
			vm.funcionarioAlterado.idSetor = funcionario.idSetor;
			$("#modal").modal("show");
			
		},
		atualiza: function(){
			const vm = this;
			if(vm.funcionarioAlterado.nome == "" || vm.funcionarioAlterado.setor == ""|| vm.funcionarioAlterado.salario == ""|| vm.funcionarioAlterado.email == ""){
				alert("Preencha todos os campos!");
				return;
			}
			axios.post("/funcionarios/rs/funcionarios/atualizaCadastro/", vm.funcionarioAlterado)
			.then(response => {
				alert("Funcionário atualizado com sucesso!");
			}).catch(function (error) {
			}).finally(function() {
				vm.listarFuncionarios();
				vm.limpaCampos();
			});
		},
		limpaCampos: function(){
			const vm = this;
			document.getElementById("nome").value = "";
			document.getElementById("salario").value = "";
			document.getElementById("email").value = "";
			
		}
    }
});