package com.hepta.funcionarios.persistence;


import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionManager implements Serializable {
	private static final long serialVersionUID = 1L;
	private static final String DB_DRIVER = "com.mysql.cj.jdbc.Driver";
 
	private static final String DB_CONNECTION = "jdbc:mysql://us-cdbr-east-04.cleardb.com/heroku_62ad4fb5b830b80?serverTimezone=America/Sao_Paulo&allowPublicKeyRetrieval=true&useSSL=false";
	private static final String DB_USER = "bfafea1634f87a";
	private static final String DB_PASSWORD = "12a929c580f127c";

	
	Connection dbConnection = null;
	
	public static Connection getDBConnection() throws Exception {
		return getDBConnection(DB_CONNECTION, DB_USER, DB_PASSWORD);
	} 	

	public static Connection getDBConnection(String conn, String user, String pass) throws Exception {
		Connection dbConnection = null;
		Class.forName(DB_DRIVER);
		dbConnection = DriverManager.getConnection(conn, user, pass);
		return dbConnection;
	}
}