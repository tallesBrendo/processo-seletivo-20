package com.hepta.funcionarios.persistence;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import com.hepta.funcionarios.dto.FuncionarioSetorDTO;
import com.hepta.funcionarios.entity.Setor;

public class FuncionarioDAO {

	public void save(FuncionarioSetorDTO func) throws Exception {
		Connection db = ConnectionManager.getDBConnection();
		PreparedStatement pstmt = null;

		StringBuilder sql = new StringBuilder();

		sql.append("INSERT INTO Funcionario ");
		sql.append(" ( ");
		sql.append(" nome, ");
		sql.append(" idSetor, ");		
		sql.append(" salario, ");
		sql.append(" email, ");
		sql.append(" dataNascimento ");
		sql.append(" ) ");
		sql.append(" VALUES (?,?,?,?,?);");

		try {
			pstmt = db.prepareStatement(sql.toString(), Statement.RETURN_GENERATED_KEYS);
			pstmt.setString(1, func.getNome());
			pstmt.setInt(2, func.getIdSetor());
			pstmt.setDouble(3, func.getSalario());
			pstmt.setString(4, func.getEmail());
			pstmt.setString(5, func.getDataNascimento());

			pstmt.executeUpdate();

		} finally {
			if (pstmt != null)
				pstmt.close();
			db.close();
		}

	}

	public ArrayList<FuncionarioSetorDTO> listarFuncionarios() throws Exception {
		Connection db = ConnectionManager.getDBConnection();
		PreparedStatement pstmt = null;

		ResultSet result = null;
		ArrayList <FuncionarioSetorDTO> lista = new ArrayList<FuncionarioSetorDTO>();

		pstmt = db.prepareStatement("select * from Funcionario f\r\n"
				+ "	inner join Setor s\r\n"
				+ "		on s.idSetor = f.idSetor");
		try {
			result = pstmt.executeQuery();
			while (result.next()) {
				FuncionarioSetorDTO dto = new FuncionarioSetorDTO();
				dto.setId(result.getInt("id"));
				dto.setNome(result.getString("nome"));
				dto.setIdSetor(result.getInt("idSetor"));
				dto.setSalario(result.getDouble("salario"));
				dto.setEmail(result.getString("email"));
				dto.setDataNascimento(result.getString("dataNascimento"));
				dto.setNomeSetor(result.getString("nomeSetor"));
				lista.add(dto);
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (pstmt != null)
				pstmt.close();
			db.close();
		}
		return lista;
	}

	public ArrayList<Setor> listarSetores() throws Exception {
		Connection db = ConnectionManager.getDBConnection();
		PreparedStatement pstmt = null;

		ResultSet result = null;
		ArrayList <Setor> lista = new ArrayList<Setor>();

		pstmt = db.prepareStatement("select * from Setor");
		try {
			result = pstmt.executeQuery();
			while (result.next()) {
				Setor setor = new Setor();
				setor.setId(result.getInt("idSetor"));
				setor.setNome(result.getString("nomeSetor"));
				lista.add(setor);
			}
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (pstmt != null)
				pstmt.close();
			db.close();
		}
		return lista;
	}

	public void apagaFuncionario(int idFuncionario) throws Exception {

		Connection db = ConnectionManager.getDBConnection();
		PreparedStatement pstmt = null;

		pstmt = db.prepareStatement("delete from Funcionario where id="+idFuncionario);

		try {
			pstmt.executeUpdate();			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (pstmt != null)
				pstmt.close();
			db.close();
		}

	}



	public void atualizaCadastro(FuncionarioSetorDTO funcionario) throws Exception {

		Connection db = ConnectionManager.getDBConnection();
		PreparedStatement pstmt = null;

		pstmt = db.prepareStatement("update Funcionario set nome='"+funcionario.getNome()+"', email='"+funcionario.getEmail()+"' , idSetor="+ funcionario.getIdSetor()+" , salario='"+funcionario.getSalario()+"' where id="+funcionario.getId());

		try {
			pstmt.executeUpdate();			
		} catch (Exception e) {
			throw new Exception(e);
		} finally {
			if (pstmt != null)
				pstmt.close();
			db.close();
		}

	}
}