package com.hepta.funcionarios.rest;

import java.util.ArrayList;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.hepta.funcionarios.dto.FuncionarioSetorDTO;
import com.hepta.funcionarios.entity.Setor;
import com.hepta.funcionarios.persistence.FuncionarioDAO;

@Path("/funcionarios")
public class FuncionarioService {

	@Context
	private HttpServletRequest request;

	@Context
	private HttpServletResponse response;

	private FuncionarioDAO dao;

	public FuncionarioService() {
		dao = new FuncionarioDAO();
	}

	protected void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	/**
	 * Adiciona novo Funcionario
	 * 
	 * @param Funcionario: Novo Funcionario
	 * @return response 200 (OK) - Conseguiu adicionar
	 */
	@Path("/cadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@POST
	public Response funcionarioCreate(FuncionarioSetorDTO dto) {
		try {
			dao.save(dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok().build();
	}
	
	@Path("/listarFuncionarios")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response listarFuncionarios() {
		
		ArrayList<FuncionarioSetorDTO> funcionarios = null;
		
		try {
			funcionarios =  dao.listarFuncionarios();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.ok().entity(funcionarios).build();
	}
	
	@Path("/todosSetores")
	@Produces(MediaType.APPLICATION_JSON)
	@GET
	public Response todosSetores() {
		ArrayList<Setor> setores = null;
		
		try {
			setores = dao.listarSetores();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.ok().entity(setores).build();
	}
	
	@Path("/apagaFuncionario/{id}")
	@Consumes(MediaType.APPLICATION_JSON)
	@GET
	public Response apagaFuncionario(@PathParam("id") int idFuncionario) {

		try {
			dao.apagaFuncionario(idFuncionario);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return Response.ok().entity("Usu�rio deletado com sucesso!").build();
	}


	@Path("/atualizaCadastro")
	@Consumes(MediaType.APPLICATION_JSON)
	@POST
	public Response atualizaCadastro(FuncionarioSetorDTO dto) {
		try {
			dao.atualizaCadastro(dto);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return Response.ok().build();
	}
	
	
	
	
	
	
	
	
	
}
