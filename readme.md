# Sobre o Projeto
O projeto consiste de um CRUD.

É necessário subir o projeto utilizando a versão 9 do tomcat.
Após subir o servidor de aplicação _(Tomcat)_ acesse o projeto via navegador.
http://localhost:8080/funcionarios/index.html

Na aba '_Lista de funcionários_' é possivel remover e editar os dados dos funcionários já cadastrados. 
Já a outra aba é utilizada para cadastro de funcionários.

**Obs: O banco de dados está hospedado em um servidor na Web, não há necessidade de preparo de qualquer tipo de ambiente para o bd.**

